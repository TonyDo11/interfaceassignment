/**
 * This represents the package geometry
 * @implements {Shape}
 */
package geometry;
import java.text.DecimalFormat;

public class Sphere implements Shape{
    private double radius;
    /**
     * Any Sphere object will have a radius
     * @param {number} radius of a sphere
     */
    public Sphere( double radius) {
        this.radius = radius;
    }
    /**
     * Get the radius of a sphere
     * @returns {number} The radius should be returned basically
     */
    public double getRadius() {
        return radius;
    }
    /**
     * Calculate the volume of the sphere
     * @throws {UnsupportedOperationException} with a message 
     */
    @Override
    public double getVolume() {
        DecimalFormat df = new DecimalFormat("#.###"); 
        double volume = (4.0/3.0) * Math.PI * (radius * radius *radius);
        double true_volume = Double.parseDouble(df.format(volume));
        return true_volume;
    }
    /**
     * Calculates the surface area of the sphere
     * @throws {UnsupportedOperationException} with a message
     */
    @Override
    public double getSurfaceArea() {
        DecimalFormat df = new DecimalFormat("#.#####");
        double area = (4.0 * Math.PI * (radius * radius));
        double true_area = Double.parseDouble(df.format(area));
        return true_area;
    }
}