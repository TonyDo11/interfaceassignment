package geometry;
public interface Shape {
    double getVolume();
    double getSurfaceArea();
}
