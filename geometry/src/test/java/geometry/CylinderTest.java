package geometry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
public class CylinderTest {


    double ErrorMargin = 0.0000001;
    @Test
    public void SAreaTest(){
        Cylinder s = new Cylinder(3, 5);
        assertEquals(48 * Math.PI, s.getSurfaceArea(),ErrorMargin);
    }
    @Test
    public void getRadius(){
        Cylinder s = new Cylinder(30, 20);
        assertEquals(30, s.getRadius(),ErrorMargin);
    }
    @Test
    public void getHeight() {
        Cylinder s = new Cylinder(30, 20);
        assertEquals(20, s.getHeight(), ErrorMargin);
    }
    @Test
    public void VolumeTest(){
        Cylinder s = new Cylinder(5, 3);
        assertEquals(75 * Math.PI, s.getVolume(),ErrorMargin);
    }

}
