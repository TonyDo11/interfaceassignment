package geometry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
public class SphereTest {
    double ErrorMargin = 0.0000001;
    @Test
    public void SAreaTest(){
        Sphere s = new Sphere(3);
        assertEquals(113.09734, s.getSurfaceArea(),ErrorMargin);
    }
    @Test
    public void getRadius(){
        Sphere s = new Sphere(30);
        assertEquals(30, s.getRadius(),ErrorMargin);
    }
    @Test
    public void VolumeTest(){
        Sphere s = new Sphere(3);
        assertEquals(113.097, s.getVolume(),ErrorMargin);
    }


}
